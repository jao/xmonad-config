-- https://gist.github.com/lierdakil/216a9d2b9e934d9e1a30

import Graphics.X11.Xlib
import Graphics.X11.Xlib.Extras
import System.Environment

main :: IO ()
main = sendCommand "XMONAD_COMMAND" =<< getArgs

sendCommand :: String -> [String] -> IO ()
sendCommand addr (s:_) = do
  d   <- openDisplay ""
  rw  <- rootWindow d $ defaultScreen d
  a <- internAtom d addr False
  m <- internAtom d s False
  allocaXEvent $ \e -> do
                  setEventType e clientMessage
                  setClientMessageEvent e rw a 32 m currentTime
                  sendEvent d rw False structureNotifyMask e
                  sync d False
sendCommand _ _ = return ()
