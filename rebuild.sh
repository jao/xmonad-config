#!/bin/bash

cabal update
rm -f .ghc.environment.*
cabal install --package-env=/home/jao/.xmonad --lib xmonad xmonad-contrib
cabal install --package-env=/home/jao/.xmonad xmonad
