#!/bin/bash

killall trayer

if [[ $JAO_COLOR_SCHEME == dark ]]; then
  alpha=255
else
  alpha=60
fi

trayer --margin 2 --distancefrom left \
       --distance 1 --edge top \
       --align left --SetDockType true --SetPartialStrut false \
       --widthtype request \
       --height 21 --heighttype pixel \
       --transparent true \
       --alpha $alpha --padding 1 &

killall xmobar-single
xmobar-single $* &
